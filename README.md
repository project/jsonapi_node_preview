# JSON:API Node Preview

A module to get node previews in JSON:API.

## INTRODUCTION ##
It adds a new endpoint for all JSON:API resources of type node, adding a
`/preview` string to the end of the original JSON:API path.

For example:
 * Node / Article : `/jsonapi/node/article/{UUID}/preview`
 * Node / Page : `/jsonapi/node/page/{UUID}/preview`
 * etc

That endpoint is compatible with any other JSON:API parameters available
(`fields`, `include`, etc).

The expected workflow to make this module work is as following:

* Edit any node and click on "Preview" button
* A new window opens, with a URL like this:
  http://example.com/node/preview/1f8b435a-0026-4628-93f3-38ae7353ffbb/full
* Grab the UUID (the "8-4-4-4-12" chars, "1f8b435a-0026-4628-93f3-38ae7353ffbb"
  in this example)
* Execute a request to `/jsonapi/node/article/1f8b435a-0026-4628-93f3-38ae7353ffbb/preview`

Being a preview operation, this endpoint:
1) is not cached.
2) is authenticated, so it is only available to users with access to node
previews.

You can use any UUID to request the preview endpoint. If that UUID has not been
previewed, or the user making the request has not enough permissions, JSON:API
responds with a 404 error.

## REQUIREMENTS ##
* Node module
* JSON:API module

## INSTALLATION ##
Run `composer require drupal/jsonapi_node_preview`.

## CONFIGURATION ##
Install it and it works. There's no configuration, no UI.

## SIMILAR MODULES ##
Visit https://www.drupal.org/project/graphql_node_preview for a version of this module for GraphQL.